<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'name' => 'required|min:5|max:255',
                'email' => 'required|min:5|max:255|email'
        ];
    }
    public function messages(){
        return [
            'name.required' => 'Le champ Name est requis',
            'email.required' => 'Le champ Email est requis',
            'email.email' => 'Le champ Email n\'est pas valide',
            'email.min' => 'Le champ Email doit faire au moins 5 caractères',
        ];
    }
}
