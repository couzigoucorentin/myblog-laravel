@extends('base')

@section('content')
<div class="bg-secondary p-2">
    <h1 class="text-center">Article n°{{$article->id}}</h1>

    <div class="col">
        <div class="card m-1 h-100">
            <div class="card-body">
                <h5 class="card-title">{{ $article->title }}</h5>
                <h6 class="card-title">{{ $article->subtitle }}</h6>
                <p class="card-text">{{ $article->content }}</p>
            </div>
            <a href="{{route('articles')}}" class=" btn btn-primary">Retour</a>
        </div>
        @endsection
