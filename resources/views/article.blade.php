@extends('base')

@section('content')
<div class="bg-secondary p-2">
    <h1 class="text-center">Article</h1>
    <div class="row row-cols-1 row-cols-md-3 g-4">
        @foreach($articles as $article)
        <div class="col">
            <div class="card m-1 h-100">
                <div class="card-body">
                    <h5 class="card-title">{{ $article->title }}</h5>
                    <h6 class="card-title">{{ $article->subtitle }}</h6>
                </div>
                <div class="card-footer">
                    <a href="{{route('article',$article->slug)}}"><button class=" btn btn-primary"><i class="fa-solid fa-arrow-right"></i> Lire la suite</button></a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <div class="d-flex justify-content-center mt-5">{{ $articles->links('vendor.pagination.custom') }}</div>
</div>
@endsection
